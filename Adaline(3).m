%Adaline
function [w] = Adaline(x, yd, ni, w)

z = w'*x;
[m n] = size(x);

for i = 1:n
    y(i)= 2*exp(-2*z(i))/(1+exp(-2*z(i)));
    %y(i)= 1/(1+exp(-2*z(i)));
end

Verro = (yd-y).^2;
Serro = sum(Verro);

while(Serro>0.01)
    for i=1:m
        w(i) = w(i) - ni*(Serro/w(i));
    end
    
    z = w'*x;
    
    for i = 1:n
        y(i)= 2*exp(-2*z(i))/(1+exp(-2*z(i)));
        %y(i)= 1/(1+exp(-2*z(i)));
    end
    
    Verro = (yd-y).^2;
    Serro = sum(Verro);
    
end